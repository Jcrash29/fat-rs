#![allow(dead_code)]
extern crate core;

// Crate declarations
#[macro_use]
extern crate bitflags;
extern crate byteorder;
#[macro_use]
extern crate derive_more;
extern crate encode_unicode;

use core::{cmp, fmt, marker, mem, result};

use encode_unicode::Utf16Char;

mod fat;
use fat::*;
pub use fat::SectorOffset;

// FIXME: Change this to switch on `no_std`
//macro_rules! println {
//    ($($t:tt)*) => ();
//}

/// Represents a single FAT32 partition.
#[derive(Debug)]
pub struct FatFileSystem<T: StorageDevice> {
    device: T,
    device_id: u8,
    bpb: BiosParameterBlock,
    /// Number of unallocated clusters
    free_cluster_count: Option<u32>,
    /// The cluster number of the next unallocated cluster
    next_free_cluster: Option<ClusterOffset>,
}

impl<T: StorageDevice> FatFileSystem<T> {
    /// Mounts the device using the specified `StorageDevice` with the specified `id`
    pub fn mount(id: u8) -> FatResult<FatFileSystem<T>> {
        // Initialize the device
        let mut device = T::initialize(id)?;

        // Read enough to decode the BPB
        let mut boot_sector: [u8; 512] = unsafe { mem::uninitialized() };
        device.read(id, &mut boot_sector, SectorOffset::from(0u8), 1)?;
        let bpb = unsafe { BiosParameterBlock::new(boot_sector) };
        bpb.is_valid();

        // Only support Fat32 here
        println!("{:?}", bpb.fat_type());
        match bpb.fat_type() {
            FatType::Fat32 => (),
            _ => return Err(FatError::UnsupportedFileSystem),
        }

        println!("{:?}", bpb);

        // Also read the FileSystemInfo sector
        let mut fsinfo_sector: [u8; 512] = unsafe { mem::uninitialized() };
        device.read(id, &mut fsinfo_sector, SectorOffset::from(1u8), 1)?;
        let fsinfo = unsafe { FileSystemInfo::new(fsinfo_sector) };

        println!("{:?}", fsinfo);

        let fs = FatFileSystem {
            device: device,
            device_id: id,
            bpb: bpb,
            free_cluster_count: fsinfo.free_cluster_count(),
            next_free_cluster: fsinfo.next_free_cluster(),
        };

        Ok(fs)
    }

    /// Returns the root directory `/` for this file system.
    pub fn root(&mut self) -> Directory<T> {
        Directory {
            first_cluster: self.bpb.root_cluster(),
            fs: self,
        }
    }

    /// Validates that the file system is internally coherent and valid.
    // FIXME:
    pub fn is_valid(&self) -> bool {
        true
    }

    fn list_entries_for_cluster(&mut self, cluster: ClusterOffset) {
        println!("Listing entries for cluster: {:?} -----------------", cluster);
        let cluster_sector_offset = self.bpb.first_sector_of_cluster(cluster);
        // We need to go through every sector in this cluster possibly
        for i in 0..self.bpb.sectors_per_cluster {
            println!("cluster sector {}", i);

            // Load in the current sector
            let mut sector: [u8; 512] = unsafe { mem::uninitialized() };
            let sector_num = cluster_sector_offset + SectorOffset::from(i as u32);
            self.device.read(self.device_id, &mut sector, sector_num, 1).unwrap();

            let dir_sector = DirectorySector::new(sector);
            for i in 0..16 {
                match dir_sector.entry(i) {
                    DirectoryEntry::Short(short_entry) => {
                        println!("Entry {}", i);
                        println!("{:?}", short_entry);

                        // Ignore the "special" dot and dotdot directory entries
                        if short_entry.name == *b".          " ||
                           short_entry.name == *b"..         " {
                            continue;
                        }

                        // Otherwise let's see if there's LFN entries for this file
                        let checksum = short_entry.checksum();
                        debug_assert!(i >= 1);
                        let mut j = i - 1;
                        loop {
                            if let DirectoryEntry::Long(long_entry) = dir_sector.entry(j) {
                                if long_entry.checksum == checksum {
                                    println!("Entry {}", j);
                                    println!("{:?}", long_entry);
                                    // The final entry in a long file name sequence will use `n | 0x40`
                                    // as it's order number. If we detect that, we should stop scanning.
                                    if !long_entry.is_last() {
                                        j -= 1;
                                    } else {
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }

                        // And if it's a directory, recurse into it
                        if short_entry.attributes.contains(DIRECTORY) {
                            self.list_entries_for_cluster(short_entry.first_cluster());
                        }
                    }

                    // Once we hit the first truly free directory entry, we can stop scanning.
                    DirectoryEntry::FirstFree => {
                        println!("ALL DONE with cluster {:?} -----------------", cluster);
                        return;
                    }
                    DirectoryEntry::Long(_) => {
                        println!("weird!");
                    }

                    // Unused don't need to be processed as there's no data in them. And Long are
                    // processed with Short's as they're always paired with one.
                    _ => (),
                }
            }
        }
    }

    pub fn list_entries(&mut self) {
        let cluster = self.bpb.root_cluster();
        self.list_entries_for_cluster(cluster);
    }
}

/// Specifies absolute or relative locations within a file.
///
/// The enum types have been chosen to support moving anywhere in a file without doing any 64-bit
/// math.
#[derive(Debug)]
pub enum SeekTo {
    /// Specifies an absolute position relative to the start of the file in bytes.
    ///
    /// This must be a location that's >= 0 and <= the file size. If this is equal to the file size,
    /// it will be at the end of the file and read()s will return no data and `at_end` will return
    /// true.
    AfterStart(u32),
    /// Indicates a relative location before the current location in bytes.
    ///
    /// This must result in a location that's >= 0.
    BeforeCurrent(u32),
    /// Indicates a relative location after the current location in bytes.
    ///
    /// This must result in a location that's <= the file size. If this is equal to the file size,
    /// it will be at the end of the file and read()s will return no data and `at_end` will return
    /// true.
    AfterCurrent(u32),
    /// Indicates a relative location before the end of file location in bytes.
    ///
    /// This must result in a location that's >= 0.
    /// Note: BeforeEnd(0) will result in the internal cursor at the end of the file
    /// and read()s will return no data and `at_end` will return true.
    BeforeEnd(u32)
}

/// Represents a file on the file system.
#[derive(Debug)]
pub struct File<'a, T: 'a + StorageDevice> {
    /// The first cluster allocaed to this file. This is stored such that seek functionality can
    /// be done.
    first_cluster: ClusterOffset,
    /// The current cluster being accessed for this file
    current_cluster: ClusterOffset,
    /// The current byte offset within the cluster. This is guaranteed to match the current
    /// `position`, though this is relative to the current cluster offset. If this value is the
    /// same as the number of bytes in a cluster, it means that the last read was finished at the
    /// cluster boundary.
    cluster_offset: ByteOffset,
    /// The current byte position within the file. Guaranteed to always be < size unless the file
    /// has been completely read, then it'll be == size.
    position: ByteOffset,
    /// The current size of the file.
    size: Bytes,
    /// The backing file system object
    fs: &'a mut FatFileSystem<T>,
}

impl<'a, T: 'a + StorageDevice> File<'a, T> {
    /// Seeks to the next cluster in the cluster chain.
    ///
    /// This doesn't actually change the current file position, as we don't actually move where we
    /// are in the file. This just cleans up the backend bookkeeping to match the current file
    /// position.
    fn seek_to_next_cluster(&mut self) -> FatResult<()> {
        // Find the next cluster. First get the sector containing the current cluster
        let fat_sector = self.fs.bpb.fat_sector_number(self.current_cluster);
        println!("Fat sector: {:?}", &fat_sector);
        let mut fat_sector_buf: [u8; 512] = unsafe { mem::uninitialized() };
        self.fs.device.read(self.fs.device_id, &mut fat_sector_buf, fat_sector, 1)?;
        let fat_sector_data = FatSector::new(fat_sector_buf);

        // Then read the entry that points to the next cluster
        let entry_num = self.fs.bpb.entry_offset_in_fat_sector(self.current_cluster);
        println!("Entry number: {:?}", &entry_num);
        self.current_cluster = match fat_sector_data.entry(entry_num as u8) {
            FatEntry::ClusterReference(x) => x,
            _ => return Err(FatError::IoError),
        };
        let cluster_byte_start = self.fs.bpb.first_sector_of_cluster(self.current_cluster) * 512;
        println!("Next cluster: {:?} ({:?} bytes)", self.current_cluster, cluster_byte_start);
        self.cluster_offset = 0u32.into();
        Ok(())
    }

    /// Reads data from the file into the provided buffer starting from the current file position.
    ///
    /// Returns the number of read bytes if successful.
    // TODO: Add a special case for not using a temporary when reading into a sector-sized
    // buffer.
    pub fn read(&mut self, buf: &mut [u8]) -> FatResult<u32> {
        // If the length of the given buffer is larger than the file, we still only read to
        // the end.
        let mut left_to_read = Into::<u32>::into(self.size) - Into::<u32>::into(self.position);
        println!("Left to read: {:?}, current position: {:?}", left_to_read, self.position);
        let read_len = cmp::min(buf.len(), left_to_read as usize);
        if read_len == 0 {
            return Ok(0u32.into());
        }

        let end_offset = Into::<u32>::into(self.cluster_offset) + (read_len as u32);
        let cluster_size = Into::<u32>::into(self.fs.bpb.size_of_cluster());
        println!("Read length: {}, end_offset: {}, cluster_size: {}", read_len, end_offset, cluster_size);

        // Previous reads may have left us at the end of a sector boundary, so seek to the next one.
        if Into::<Bytes>::into(self.cluster_offset) == self.fs.bpb.size_of_cluster() {
            println!("Seek to the start of the next cluster");
            self.seek_to_next_cluster()?;
        }

        // Read the first sector. Since `cluster_offset` is always within the current cluster range,
        // we don't need to worry about scanning the cluster chain for this sector read.
        let sector_offset = Into::<u32>::into(self.cluster_offset) / 512u32;
        let first_cluster_sector = self.fs.bpb.first_sector_of_cluster(self.current_cluster);
        let mut start_sector = first_cluster_sector + sector_offset.into();
        println!("First sector: {:?} + {:?} = {:?}", first_cluster_sector, sector_offset, start_sector);
        let mut sector_buf: [u8; 512] = unsafe { mem::uninitialized() };
        self.fs.device.read(self.fs.device_id, &mut sector_buf, start_sector, 1)?;
        start_sector += 1u32.into();

        // Copy the desired portion into the output buffer.
        let copy_start = (Into::<u32>::into(self.cluster_offset) % 512u32) as usize;
        let copy_end = cmp::min(copy_start + read_len, 512);
        let mut buf_start = 0;
        let buf_end = copy_end - copy_start;
        println!("Copying to buf[..{}] from sector_buf[{}..{}]", buf_end, copy_start, copy_end);
        buf[buf_start..buf_end].copy_from_slice(&sector_buf[copy_start..copy_end]);
        buf_start = buf_end;
        left_to_read = (read_len - buf_end) as u32;
        self.cluster_offset += buf_end.into();
        debug_assert!(Into::<Bytes>::into(self.cluster_offset) <= self.fs.bpb.size_of_cluster());
        self.position += buf_end.into();

        // Now read all intermediate whole sectors into the output buffer.
        if left_to_read >= 512 {
            let mut whole_sectors_left = left_to_read / 512;
            println!("Still {:?} whole sectors ({:?} bytes) to read after the first sector", whole_sectors_left, left_to_read);
            // Finish all sector reads within the current cluster.
            if sector_offset + 1 < self.fs.bpb.sectors_per_cluster as u32 {
                let sectors_to_read = cmp::min(whole_sectors_left, (self.fs.bpb.sectors_per_cluster as u32) - (sector_offset + 1)) as u8;
                let buf_end = buf_start + (sectors_to_read as usize) * 512usize;
                println!("Reading current cluster of intermediate sectors [{:?}..+{}] into buf[{}..{}]", start_sector, sectors_to_read, buf_start, buf_end);
                self.fs.device.read(self.fs.device_id, &mut buf[buf_start..buf_end], start_sector, sectors_to_read)?;
                start_sector += sectors_to_read.into();
                let bytes_read = Into::<u32>::into(sectors_to_read) * 512u32;
                left_to_read -= bytes_read;
                buf_start = buf_end;
                whole_sectors_left -= sectors_to_read as u32;
                self.cluster_offset += bytes_read.into();
                debug_assert!(Into::<Bytes>::into(self.cluster_offset) <= self.fs.bpb.size_of_cluster());
                self.position += bytes_read.into();
            }
            // Loop through all subsequent sector reads 1 cluster at a time.
            while whole_sectors_left > 0 {
                println!("whole sectors_left: {}", whole_sectors_left);
                // At this point we should be at the end of a cluster boundary, so seek to the next
                // one.
                debug_assert_eq!(Into::<u32>::into(self.cluster_offset), Into::<u32>::into(self.fs.bpb.size_of_cluster()));
                self.seek_to_next_cluster()?;

                // Now read all of the sectors that should be read from this cluster
                let sectors_to_read = cmp::min(whole_sectors_left, (self.fs.bpb.sectors_per_cluster as u32)) as u8;
                let buf_end = buf_start + (sectors_to_read as usize) * 512usize;
                println!("Reading current cluster of intermediate sectors [{:?}..+{}] into buf[{}..{}]", start_sector, sectors_to_read, buf_start, buf_end);
                self.fs.device.read(self.fs.device_id, &mut buf[buf_start..buf_end], start_sector, sectors_to_read)?;
                start_sector += sectors_to_read.into();
                let bytes_read = Into::<u32>::into(sectors_to_read) * 512u32;
                left_to_read -= bytes_read;
                buf_start = buf_end;
                whole_sectors_left -= sectors_to_read as u32;
                self.cluster_offset += bytes_read.into();
                debug_assert!(Into::<Bytes>::into(self.cluster_offset) <= self.fs.bpb.size_of_cluster());
                self.position += bytes_read.into();
            }
        }

        // We're done here unless the intermediate whole sector reads above didn't end
        // on a sector boundary. So account for that with a final partial sector read.
        if left_to_read > 0 {
            debug_assert!(left_to_read < 512);
            // Read in a whole sector to the buffer
            println!("Final partial sector read for sector {:?}", start_sector);
            self.fs.device.read(self.fs.device_id, &mut sector_buf, start_sector, 1)?;

            // Then copy the end portion to the output buffer
            let copy_end = left_to_read as usize;
            println!("Copying to buf[{}..] from sector_buf[..{}]", buf_start, copy_end);
            buf[buf_start..].copy_from_slice(&sector_buf[..copy_end]);
            self.cluster_offset += left_to_read.into();
            self.position += left_to_read.into();
            debug_assert!(Into::<Bytes>::into(self.cluster_offset) <= self.fs.bpb.size_of_cluster());
        }

        Ok(read_len as u32)
    }

    /// Place the current file position pointer at the specified byte offset.
    pub fn seek(&mut self, position: SeekTo) -> FatResult<()> {
        println!("Attempting to seek to {:?} in {:?} file", position, self.size);

        // Convert the provided position into an absolute position and make sure it's within the
        // file.
        let position = match position {
            SeekTo::AfterStart(x) => {
                if x > Into::<u32>::into(self.size) {
                    return Err(FatError::InvalidArgument);
                }
                x
            },
            SeekTo::BeforeCurrent(x) => {
                if let Some(x) = Into::<u32>::into(self.position).checked_sub(x) {
                    // `x` is guaranteed to be >= 0 at this point.
                    x
                } else {
                    return Err(FatError::InvalidArgument);
                }
            },
            SeekTo::AfterCurrent(x) => {
                if let Some(x) = Into::<u32>::into(self.position).checked_add(x) {
                    if x <= Into::<u32>::into(self.size) {
                        x
                    } else {
                        return Err(FatError::InvalidArgument);
                    }
                } else {
                    return Err(FatError::InvalidArgument);
                }
            }
            SeekTo::BeforeEnd(x) => {
                if x <= Into::<u32>::into(self.size) {
                    // `x` guaranteed to be less than the size of the file
                    Into::<u32>::into(self.size) - x
                } else {
                    return Err(FatError::InvalidArgument);
                }
            }
        };

        // If the new position is still in the same cluster, there's no need to follow the cluster
        // chain. There is a special case with `cluster_offset` where if a read ends on a cluster
        // boundary then the value will be into the next cluster. We subtract 1 to keep it in the
        // current cluster offset range before the operation in that case.
        let cluster_start = if Into::<u32>::into(self.cluster_offset) == Into::<u32>::into(self.fs.bpb.size_of_cluster()) {
            ((Into::<u32>::into(self.position) - 1) /
                Into::<u32>::into(self.fs.bpb.size_of_cluster())) *
                Into::<u32>::into(self.fs.bpb.size_of_cluster())
        } else {
            (Into::<u32>::into(self.position ) /
                Into::<u32>::into(self.fs.bpb.size_of_cluster())) *
                Into::<u32>::into(self.fs.bpb.size_of_cluster())
        };
        let cluster_end: u32 = cluster_start + Into::<u32>::into(self.fs.bpb.size_of_cluster());
        println!("Checking if position within current cluster [{}..{}]", cluster_start, cluster_end);
        if position >= cluster_start && position <= cluster_end {
            // Calculate the location relative to the current position.
            if position > Into::<u32>::into(self.position) {
                let diff = position - Into::<u32>::into(self.position);
                self.position += diff.into();
                self.cluster_offset += diff.into();
            } else {
                let diff = Into::<u32>::into(self.position) - position;
                self.position -= diff.into();
                self.cluster_offset -= diff.into();
            };
            Ok(())
        }
        // At this point we know that we'll need to traverse the cluster chain.
        else {
            // The only difference between traversing the cluster chain to seek before or after the
            // current cluster is what the start cluster is. The current cluster is used when
            // seeking forward, otherwise we start from the beginning.
            if position < Into::<u32>::into(self.position) {
                self.current_cluster = self.first_cluster;
            }

            // Iterate through the cluster chain until we're within a cluster's-width of the desired
            // location.
            let cluster_start = if position > Into::<u32>::into(self.fs.bpb.size_of_cluster()) {
                position - Into::<u32>::into(self.fs.bpb.size_of_cluster())
            } else {
                0
            };
            while Into::<u32>::into(self.position) < cluster_start {
                self.seek_to_next_cluster()?;
                self.position += self.fs.bpb.size_of_cluster().into();
            }

            // Once we're within the currect cluster, set all the necessary offsets to their correct
            // values.
            let diff = position % Into::<u32>::into(self.fs.bpb.size_of_cluster());
            println!("New position is {} into the current cluster", diff);
            self.cluster_offset = diff.into();
            self.position = position.into();
            Ok(())
        }
    }

    /// Returns the size of the file in bytes.
    pub fn size(&self) -> u32 {
        self.size.into()
    }

    /// Checks if the file is currently pointing at the end.
    pub fn at_end(&self) -> bool {
        Into::<Bytes>::into(self.position) == self.size
    }
}

/// Represents a directory on the file system.
#[derive(Debug)]
pub struct Directory<'a, T: 'a + StorageDevice> {
    /// The first cluster for this directory
    first_cluster: ClusterOffset,
    fs: &'a mut FatFileSystem<T>,
}

impl<'a, T: 'a + StorageDevice> Directory<'a, T> {
    /// Indicates whether the file exists. Assumes all searches are relative to this directory.
    fn file_exists(&mut self, filepath: &str) -> FatResult<Option<DirectoryEntryShort>> {
        // First load in the first sector for the root cluster
        let mut sector: [u8; 512] = unsafe { mem::uninitialized() };
        let first_sector = self.fs.bpb.first_sector_of_cluster(self.first_cluster);
        self.fs.device.read(self.fs.device_id, &mut sector, first_sector, 1)?;

        let mut filepath_index;
        let dir_sector = DirectorySector::new(sector);
        debug_assert!(!filepath.is_empty());
        for i in 0..16 {
            if let DirectoryEntry::Short(&short_entry) = dir_sector.entry(i) {
                println!("{:?}", short_entry);
                // Check that the short name for this file could possibly match
                match short_name_compare(&short_entry.name, filepath.as_bytes()) {
                    FilenameMatch::Exact => {
                        println!("Exact match found!");
                        return Ok(Some(short_entry));
                    }
                    FilenameMatch::Possible => {
                        println!("Possible match found, searching LFN");
                        // Match against the entire filename by searching along the LFNs
                        let checksum = short_entry.checksum();
                        debug_assert!(i >= 1);
                        let mut j = i - 1;
                        filepath_index = 0usize;
                        'outer: loop {
                            if let DirectoryEntry::Long(long_entry) = dir_sector.entry(j) {
                                if long_entry.checksum == checksum {
                                    println!("{:?}", long_entry);
                                    for i in 0..long_entry.name1.len() {
                                        let x = Utf16Char::from_slice_start(&long_entry.name1[i..i+1]).unwrap().0.to_char();
                                        let search_char = filepath.chars().nth(filepath_index).unwrap();
                                        filepath_index += 1;
                                        println!("Matching x and search_char ({}, {})", x, search_char);

                                        // If we've run out of characters in the LFN or the
                                        // character doesn't match our search, we
                                        if long_entry.name1[i] != 0 &&
                                           long_entry.name1[i] != 0xFFFF &&
                                           x != search_char {
                                            break 'outer;
                                        }

                                        // Now if this is the last character in the search string,
                                        // and since we haven't found a match failure yet, we've
                                        // successfully found our match!
                                        if filepath_index == filepath.len() {
                                            return Ok(Some(short_entry));
                                        }
                                    }
                                    for i in 0..long_entry.name2.len() {
                                        let x = Utf16Char::from_slice_start(&long_entry.name2[i..i+1]).unwrap().0.to_char();
                                        let search_char = filepath.chars().nth(filepath_index).unwrap();
                                        filepath_index += 1;
                                        println!("Matching x and search_char ({}, {})", x, search_char);

                                        // If we've run out of characters in the LFN or the
                                        // character doesn't match our search, we
                                        if long_entry.name2[i] != 0 &&
                                           long_entry.name2[i] != 0xFFFF &&
                                           x != search_char {
                                            break 'outer;
                                        }

                                        // Now if this is the last character in the search string,
                                        // and since we haven't found a match failure yet, we've
                                        // successfully found our match!
                                        if filepath_index == filepath.len() {
                                            return Ok(Some(short_entry));
                                        }
                                    }
                                    for i in 0..long_entry.name3.len() {
                                        let x = Utf16Char::from_slice_start(&long_entry.name3[i..i+1]).unwrap().0.to_char();
                                        let search_char = filepath.chars().nth(filepath_index).unwrap();
                                        filepath_index += 1;
                                        println!("Matching x and search_char ({}, {})", x, search_char);

                                        // If we've run out of characters in the LFN or the
                                        // character doesn't match our search, we
                                        if long_entry.name3[i] != 0 &&
                                           long_entry.name3[i] != 0xFFFF &&
                                           x != search_char {
                                            break 'outer;
                                        }

                                        // Now if this is the last character in the search string,
                                        // and since we haven't found a match failure yet, we've
                                        // successfully found our match!
                                        if filepath_index == filepath.len() {
                                            return Ok(Some(short_entry));
                                        }
                                    }

                                    // If we aren't the last entry and are still matching, move
                                    // on to the next LFN.
                                    if !long_entry.is_last() {
                                        j -= 1;
                                    }
                                } else {
                                    break;
                                }
                            }
                            // Should never get to this point
                            else {
                                unreachable!();
                            }
                        }
                        println!("done!");
                    }
                    FilenameMatch::None => {
                        println!("Not this one!");
                        continue
                    }
                }
            }
        }

        // As long as an error hasn't happened, then report that we didn't find the file.
        Ok(None)
    }

    /// Opens an existing directory.
    pub fn open_directory(&mut self, path: &str) -> FatResult<Option<Directory<T>>> {
        if let Some(dir) = self.file_exists(path)? {
            if dir.is_directory() {
                return Ok(Some(Directory {
                    first_cluster: dir.first_cluster(),
                    fs: self.fs,
                }));
            }
        }
        Ok(None)
    }

    /// Opens a file for reading and writing.
    pub fn open_file(&mut self, path: &str) -> FatResult<Option<File<T>>> {
        if let Some(file) = self.file_exists(path)? {
            if !file.is_directory() {
                return Ok(Some(File {
                    first_cluster: file.first_cluster(),
                    current_cluster: file.first_cluster(),
                    cluster_offset: 0u32.into(),
                    position: 0u32.into(),
                    size: file.file_size.into(),
                    fs: self.fs,
                }));
            }
        }
        Ok(None)
    }
}

/// Indicates the status of the device.
///
/// Not all flags are necessarily in use by every device.
#[derive(Clone, Copy, Debug)]
pub enum DeviceStatus {
    /// The device is operating normally with read/write access.
    Normal,
    /// The device relies on removeable media and there is no readable disk inserted.
    NoDisk,
    /// The drive has been set to a read-only mode.
    ReadOnly,
}

/// This trait describes an interface for interacting with the low-level storage media.
///
/// For example, this could be implemented for a virtual file system (see `examples/parse_fat32.rs`)
/// or for an SD card peripheral on a microcontroller.
pub trait StorageDevice: fmt::Debug {
    /// Implements any hardware or software initialization necessary for the device.
    ///
    /// `id` is the id of the specific hardware peripheral. For example, if multiple hard drives are
    /// attached, this number would be used to select between them.
    fn initialize(id: u8) -> FatResult<Self> where Self: marker::Sized;

    /// Returns the status of the specific hardware device.
    fn status(&self, id: u8) -> FatResult<DeviceStatus>;

    /// Reads the specified sectors from the device.
    // TODO: Figure out how to unify the number of sectors to read with the buffer length
    fn read(&mut self, id: u8, buffer: &mut [u8], sector: SectorOffset, num_sectors: u8) -> FatResult<()>;

    /// Writes the specified sectors to the device.
    // TODO: Figure out how to unify the number of sectors to write with the buffer length
    fn write(&mut self, id: u8, buffer: &[u8], sector: SectorOffset, num_sectors: u8) -> FatResult<()>;
}

/// The unified error type for fat-rs.
#[derive(Clone, Copy, Debug)]
pub enum FatError {
    /// The specified device ID doesn't exist.
    InvalidDeviceId,
    /// An error occurred while trying to operate on the given device.
    IoError,
    /// The filesystem is not a valid FAT32 partition.
    UnsupportedFileSystem,
    /// The specified arguments were not valid
    InvalidArgument,
}

/// Helper type for reporting errors.
pub type FatResult<T> = result::Result<T, FatError>;

/// Rounds the given value down to the nearest multiple of 512
fn round_down_to_nearest_512(x: u32) -> u32 {
    x & 0xFE00
}
