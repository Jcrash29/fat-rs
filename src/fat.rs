use core::{fmt, mem};

use byteorder::{ByteOrder, LittleEndian};
use encode_unicode::{Utf16Char, Utf8Char};

// FIXME: Change this to switch on `no_std`
//macro_rules! println {
//    ($($t:tt)*) => ();
//}

/// Cluster index into the data clusters region.
///
/// This is a simple wrapper type around `u32`.
// TODO: Add validation that this is a valid cluster offset
#[derive(Add, AddAssign, Clone, Copy, Debug, Eq, From, Into, Mul, PartialEq)]
pub struct ClusterOffset(u32);

/// Sector index into a region of sectors.
///
/// This could be relative to the beginning of a cluster or of the entire partition. This
/// is a simple wrapper type around `u32`.
// TODO: Add validation that this is a valid sector offset
#[derive(Add, AddAssign, Clone, Copy, Debug, Eq, From, Into, Mul, PartialEq)]
pub struct SectorOffset(u32);

impl From<u8> for SectorOffset {
    fn from(data: u8) -> Self {
        SectorOffset(data as u32)
    }
}

/// The size of something in bytes.
///
/// This is a simple wrapper type around `u32`.
#[derive(Add, AddAssign, Clone, Copy, Debug, Eq, From, Into, Mul, PartialEq, PartialOrd, Sub)]
pub struct Bytes(u32);

impl From<usize> for Bytes {
    fn from(data: usize) -> Self {
        Bytes(data as u32)
    }
}

impl From<Bytes> for usize {
    fn from(data: Bytes) -> Self {
        data.into()
    }
}

impl From<ByteOffset> for Bytes {
    fn from(data: ByteOffset) -> Self {
        Bytes(data.into())
    }
}

// TODO: Add validation that this is a valid byte offset
#[derive(Add, AddAssign, Clone, Copy, Debug, Div, Eq, From, Into, Rem, PartialEq, SubAssign)]
pub struct ByteOffset(u32);

impl From<usize> for ByteOffset {
    fn from(data: usize) -> Self {
        ByteOffset(data as u32)
    }
}

impl From<Bytes> for ByteOffset {
    fn from(data: Bytes) -> Self {
        ByteOffset(data.into())
    }
}

#[derive(Clone, Copy, Debug)]
pub enum FatType {
    Fat12,
    Fat16,
    Fat32,
    ExFat,
}

#[repr(packed)]
#[derive(Clone, Copy, Debug)]
pub struct FatVersion {
    pub major: u8,
    pub minor: u8,
}

#[repr(packed)]
pub struct FileSystemInfo {
    /// Signature to indicate this is an FSInfo struct. Always 0x41615252.
    lead_signature: u32,
    reserved1: [u8; 480],
    /// Another signature to indicate this is an FSInfo struct. Always 0x61417272.
    struct_signature: u32,
    /// Last known free cluster count.  All 1s indicates that the value isn't known.
    pub free_cluster_count: u32,
    /// Cluster number of the first available cluster on the volume. All 1s indicates that this
    /// isn't known.
    pub next_free_cluster: u32,
    reserved2: [u8; 12],
    /// Trailing signature used to indicate this is an FSInfo struct. Always 0xAA550000.
    trail_signature: u32,
}

impl FileSystemInfo {
    /// Create a new FileSystemInfo from a byte array
    pub unsafe fn new(data: [u8; 512]) -> Self {
        mem::transmute(data)
    }

    pub fn is_valid(&self) -> bool {
        // All signatures must be valid
        if self.lead_signature != 0x41615252 || self.struct_signature != 0x61417272 ||
           self.trail_signature != 0xAA550000 {
            return false;
        }

        // All bytes within the reserved spots must be 0
        if !self.reserved1.iter().all(|c| *c == 0) {
            return false
        }
        if !self.reserved2.iter().all(|c| *c == 0) {
            return false
        }

        true
    }

    pub fn free_cluster_count(&self) -> Option<u32> {
        if self.free_cluster_count != 0xFFFFFFFF {
            Some(self.free_cluster_count)
        } else {
            None
        }
    }

    pub fn next_free_cluster(&self) -> Option<ClusterOffset> {
        if self.next_free_cluster != 0xFFFFFFFF {
            Some(ClusterOffset::from(self.next_free_cluster))
        } else {
            None
        }
    }
}

impl fmt::Debug for FileSystemInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "FileSystemInfo {{\n")?;

        if self.free_cluster_count != 0xFFFFFFFF {
            write!(f, "  free_cluster_count: {}\n", self.free_cluster_count)?;
        } else {
            write!(f, "  free_cluster_count: UNKNOWN\n")?;
        }

        if self.next_free_cluster != 0xFFFFFFFF {
            write!(f, "  next_free_cluster: {}\n", self.next_free_cluster)?;
        } else {
            write!(f, "  next_free_cluster: UNKNOWN\n")?;
        }

        write!(f, "}}")
    }
}

bitflags!{
    pub struct FileAttributes: u8 {
        /// The file cannot be modified. All modification requests must fail with an appropriate
        /// error code value.
        const READ_ONLY      = 0x01;
        /// The corresponding file or sub-directory must not be listed unless a request is issued by
        /// the user/application explicitly requesting inclusion of "hidden files".
        const HIDDEN         = 0x02;
        /// The corresponding file is tagged as a component of the operating system. It must not be
        /// listed unless a request is issued by the user/application explicitly requesting
        /// inclusion of "system files".
        const SYSTEM         = 0x04;
        /// The corresponding entry contains the volume label. `first_cluster_*` must always be 0
        /// for the corresponding entry (representing the volume label) since no clusters can be
        /// allocated for this entry. Only the root directory can contain one entry with this
        /// attribute. No sub-directory must contain an entry of this type. Entries representing
        /// long file names are exceptions to these rules.
        const VOLUME_ID      = 0x08;
        /// The corresponding entry represents a directory. `file_size` must always be 0.
        const DIRECTORY      = 0x10;
        /// This attribute must be set when the file is created, renamed, or modified. The presence
        /// of this attribute indicates that properties of the associate file have been modified.
        /// Backup utilities can utilize this information to determine the set of files that need to
        /// be backed up to ensure protection in case of media and other failure conditions.
        const ARCHIVE        = 0x20;
        /// This combination of attributes indicates that this entry contains part of a long file
        /// name.
        const LONG_NAME      = READ_ONLY.bits | HIDDEN.bits | SYSTEM.bits | VOLUME_ID.bits;
    }
}

pub enum DirectoryEntry<'a> {
    /// There is no directory entry here, it's free.
    Unused,
    /// This directory entry is empty and all subsequent entries are empty as well
    FirstFree,
    Short(&'a DirectoryEntryShort),
    Long(&'a DirectoryEntryLong),
}

#[derive(Clone, Copy)]
#[repr(packed)]
pub struct DirectoryEntryShort {
    /// First byte is 0xE5 when it's unused, 0x00 when it's the end of a directory
    pub name: [u8; 11],
    pub attributes: FileAttributes,
    nt_reserved: u8,
    pub creation_time_tenth: u8,
    pub creation_time: u16,
    pub creation_date: u16,
    pub last_access_date: u16,
    pub first_cluster_high: u16,
    pub last_write_time: u16,
    pub last_write_date: u16,
    pub first_cluster_low: u16,
    pub file_size: u32,
}

impl DirectoryEntryShort {

    /// Create a new DirectoryEntryShort from a byte array
    unsafe fn new(data: &[u8]) -> &Self {
        &*(data.as_ptr() as *const _)
    }

    /// Validate a short directory entry. This merely confirms that this directory entry is valid in
    /// accordance with the FAT32 standard.
    pub fn is_valid(&self) -> bool {
        // Validate name1 field
        for i in 0..self.name.len() {
            match self.name[i] {
                // These all have special meaning for the first character, but they also shouldn't be
                // valid for any other character as well.
                0xE5 | 0x00 | 0x05 if i != 0 => return false,
                // No characters can be under 0x20 except for the above
                c if c < 0x20 => return false,
                // These special characters are explicitly not allowed
                0x22 | 0x2A ... 0x2F | 0x3A ... 0x3F | 0x5B ... 0x5D | 0x7C => return false,
                // Lowercase letters are not allowed
                0x61 ... 0x7A => return false,
                _ => (),
            }
        }
        // Validate attribute. Highest 2 bits should always be 0.
        if self.attributes.bits() & 0xC != 0 {
            return false;
        }
        // And if it's a directory, its file size should be 0.
        else if self.is_directory() && self.file_size != 0 {
            return false;
        }
        // nt_reserved attribute. This should never be anything other than 0.
        if self.nt_reserved != 0 {
            return false;
        }
        true
    }

    /// Generates the checksum used by long directory entries.
    pub fn checksum(&self) -> u8 {
        short_name_checksum(&self.name)
    }

    pub fn first_cluster(&self) -> ClusterOffset {
        ClusterOffset(((self.first_cluster_high as u32) << 8u32) | (self.first_cluster_low as u32))
    }

    pub fn is_directory(&self) -> bool {
        self.attributes.contains(DIRECTORY)
    }
}

impl fmt::Debug for DirectoryEntryShort {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DirectoryEntryShort {{\n")?;

        write!(f, "  name: \"")?;
        for i in 0..8 {
            if self.name[i] != 0 && self.name[i] != 0xFF && self.name[i] != 0x20 {
                let x = Utf8Char::from_slice_start(&self.name[i..i+1]).unwrap().0;
                write!(f, "{}", x.to_char())?;
            } else {
                break;
            }
        }
        let mut added_ext_period = false;
        for i in 8..11 {
            if self.name[i] != 0 && self.name[i] != 0xFF && self.name[i] != 0x20 {
                let x = Utf8Char::from_slice_start(&self.name[i..i+1]).unwrap().0;
                if !added_ext_period {
                    write!(f, ".")?;
                    added_ext_period = true;
                }
                write!(f, "{}", x.to_char())?;
            } else {
                break;
            }
        }

        write!(f, "\"\n")?;

        write!(f, "  attributes: {:?}\n", self.attributes)?;

        write!(f, "  file size: {}\n", self.file_size)?;

        if self.file_size != 0 || self.attributes.contains(DIRECTORY) {
            write!(f, "  first_cluster: {}\n", Into::<u32>::into(self.first_cluster()))?;
        }

        write!(f, "}}")
    }
}

#[derive(Clone, Copy)]
#[repr(packed)]
pub struct DirectoryEntryLong {
    /// From 1 increasing until the last entry which is N | LAST_LONG_ENTRY (0x40)
    order: u8,
    pub name1: [u16; 5],
    pub attributes: FileAttributes,
    pub type_: u8,
    pub checksum: u8,
    pub name2: [u16; 6],
    first_cluster: u16,
    pub name3: [u16; 2],
}

impl DirectoryEntryLong {
    /// Create a new DirectoryEntryShort from a byte array
    unsafe fn new(data: &[u8]) -> &Self {
        &*(data.as_ptr() as *const _)
    }

    /// Validate a long directory entry. This merely confirms that this directory entry is valid in
    /// accordance with the FAT32 standard.
    pub fn is_valid(&self) -> bool {
        if self.first_cluster != 0 {
            return false;
        }

        true
    }

    /// If this entry is the last one in a sequence
    pub fn is_last(&self) -> bool {
        self.order & 0x40 != 0
    }

    /// The sequence order of this entry.
    ///
    /// This is a function because there's a bitmask used to specify this as the last entry in the
    /// sequence.
    pub fn order(&self) -> u8 {
        if self.is_last() {
            self.order ^ 0x40
        } else {
            self.order
        }
    }
}

impl fmt::Debug for DirectoryEntryLong {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DirectoryEntryLong {{\n")?;

        write!(f, "  name: \"")?;
        let mut more_characters = true;
        for i in 0..self.name1.len() {
            if self.name1[i] != 0 && self.name1[i] != 0xFF {
                let x = Utf16Char::from_slice_start(&self.name1[i..i+1]).unwrap().0;
                write!(f, "{}", x.to_char())?;
            } else {
                more_characters = false;
                break;
            }
        }
        if more_characters {
            for i in 0..self.name2.len() {
                if self.name2[i] != 0 && self.name2[i] != 0xFF {
                    let x = Utf16Char::from_slice_start(&self.name2[i..i+1]).unwrap().0;
                    write!(f, "{}", x.to_char())?;
                } else {
                    more_characters = false;
                    break;
                }
            }
        }
        if more_characters {
            for i in 0..self.name3.len() {
                if self.name3[i] != 0 && self.name3[i] != 0xFF {
                    let x = Utf16Char::from_slice_start(&self.name3[i..i+1]).unwrap().0;
                    write!(f, "{}", x.to_char())?;
                } else {
                    break;
                }
            }
        }
        write!(f, "\"\n")?;

        if self.is_last() {
            write!(f, "  order: {} (last),\n", self.order())?;
        } else {
            write!(f, "  order: {},\n", self.order)?;
        }

        write!(f, "  attributes: {:?}\n", self.attributes)?;

        write!(f, "  type: {}\n", self.type_)?;

        write!(f, "  checksum: 0x{:X}\n", self.checksum)?;

        write!(f, "}}")
    }
}

/// The Boot Sector/BIOS Parameter Block for FAT32
#[repr(packed)]
pub struct BiosParameterBlock {
    pub jump_boot: [u8; 3],
    pub oem_name: [u8; 8],
    pub bytes_per_sector: u16,
    pub sectors_per_cluster: u8,
    pub reserved_sectors: u16,
    pub number_of_fats: u8,
    pub root_entries: u16,
    pub total_sectors_16: u16,
    pub media: u8,
    pub fat_size_16: u16,
    pub sectors_per_track: u16,
    pub number_of_heads: u16,
    pub hidden_sectors: u32,
    pub total_sectors_32: u32,
    // FAT32 differences start here
    pub fat_size_32: u32,
    pub flags: u16, // TODO: Replace with bitfield type
    pub fat_version: FatVersion,
    pub root_cluster: u32,
    pub fsinfo_sector: u16,
    pub backup_boot_sector: u16,
    reserved: [u8; 12],
    pub drive_number: u8,
    reserved1: u8,
    pub boot_signature: u8,
    pub volume_id: u32,
    pub volume_label: [u8; 11],
    pub file_system_type: [u8; 8],
    reserved2: [u8; 420],
    trail_signature: [u8; 2],
}

impl BiosParameterBlock {
    pub fn root_cluster(&self) -> ClusterOffset {
        ClusterOffset(self.root_cluster)
    }
}

impl fmt::Debug for BiosParameterBlock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BiosParameterBlock {{\n")?;

        write!(f, "  OEM name: \"")?;
        for i in 0..self.oem_name.len() {
            if self.oem_name[i] != 0 && self.oem_name[i] != 0xFF {
                let x = Utf8Char::from_slice_start(&self.oem_name[i..i+1]).unwrap().0;
                write!(f, "{}", x.to_char())?;
            } else {
                break;
            }
        }
        write!(f, "\"\n")?;

        write!(f, "  bytes per sector: {}\n", self.bytes_per_sector)?;

        write!(f, "  sectors per cluster: {}\n", self.sectors_per_cluster)?;

        write!(f, "  reserved sectors: {}\n", self.reserved_sectors)?;

        write!(f, "  number of FATs: {}\n", self.number_of_fats)?;

        write!(f, "  total sectors: {}\n", self.total_sectors_32)?;

        write!(f, "  fat size (sectors): {}\n", self.fat_size_32)?;

        write!(f, "  drive number: {}\n", self.drive_number)?;

        write!(f, "  volume id: {}\n", self.volume_id)?;

        write!(f, "  volume label: \"")?;
        for i in 0..self.volume_label.len() {
            if self.volume_label[i] != 0 && self.volume_label[i] != 0xFF {
                let x = Utf8Char::from_slice_start(&self.volume_label[i..i+1]).unwrap().0;
                write!(f, "{}", x.to_char())?;
            } else {
                break;
            }
        }
        write!(f, "\"\n")?;

        write!(f, "  file system type: \"")?;
        for i in 0..self.file_system_type.len() {
            if self.file_system_type[i] != 0 && self.file_system_type[i] != 0xFF {
                let x = Utf8Char::from_slice_start(&self.file_system_type[i..i+1]).unwrap().0;
                write!(f, "{}", x.to_char())?;
            } else {
                break;
            }
        }
        write!(f, "\"\n")?;

        write!(f, "------------------------\n")?;

        write!(f, "  root cluster start (bytes): {:?}\n", self.first_root_cluster_bytes())?;

        write!(f, "}}")
    }
}

impl BiosParameterBlock {

    pub unsafe fn new(data: [u8; 512]) -> Self {
        mem::transmute(data)
    }

    pub fn is_valid(&self) -> bool {
        match self.bytes_per_sector {
            512 | 1024 | 2048 | 409 => (),
            _ => return false,
        }
        if !self.sectors_per_cluster.is_power_of_two() {
            return false;
        }
        if self.reserved_sectors == 0 {
            return false;
        }
        // At least 1 field is required. 2 is the convention and generally-used value. While other
        // values are valid some software may not work with values other than 2.
        if self.number_of_fats == 0 {
            return false;
        }

        // For FAT32 the root entries field must be 0.
        if self.root_entries != 0 {
            return false;
        }

        // For FAT32, the total_sectors_16 field must be 0 as the total_sectors_32 is used instead.
        if self.total_sectors_16 != 0 {
            return false;
        }

        // Must be one of [0xF0, 0xF8-0xFF] and match the first byte in jump_boot.
        match self.media {
            m @ 0xF0 | m @ 0xF8 ... 0xFF => {
                if m != self.jump_boot[0] {
                    return false;
                }
            },
            _ => return false,
        }

        // Must be 0 for FAT32 partitions. fat_size_32 is used instead
        if self.fat_size_16 != 0 {
            return false;
        }

        // No validation for sectors_per_track
        // No validation for number_of_heads
        // No validation for hidden_sectors

        // Must be non-zero for FAT32 partitions.
        if self.total_sectors_32 == 0 {
            return false;
        }

        // Must be non-zero for FAT32 partitions.
        if self.fat_size_32 != 0 {
            return false;
        }

        // Reserved bits must not be set
        if self.flags & 0xFF0 != 0 {
            return false;
        }

        // No validation for fat_version
        // No validation for root_cluster
        // No validation for fsinfo_sector

        // No other value than 6 is recommended.
        if self.backup_boot_sector != 6 {
            return false;
        }

        // All reserved fields should be 0
        if !self.reserved.iter().all(|&x| x == 0) || self.reserved1 != 0 {
            return false;
        }

        // TODO: Add validation for drive_number

        // Boot signature should be 0x29
        if self.boot_signature != 0x29 {
            return false;
        }

        // No validation for volume_id
        // No validation for volume_label

        // Must be "FAT32    "
        if self.file_system_type != *b"FAT32   " {
            return false;
        }

        // Check sector signature
        if self.trail_signature != [0x55, 0xAA] {
            return false;
        }

        true
    }

    pub fn size_of_cluster(&self) -> Bytes {
        Bytes(self.bytes_per_sector as u32 * self.sectors_per_cluster as u32)
    }

    pub fn first_data_sector(&self) -> SectorOffset {
        SectorOffset(self.reserved_sectors as u32 + (self.number_of_fats as u32 * self.fat_size_32))
    }

    /// Get the start of a cluster
    pub fn first_sector_of_cluster(&self, cluster: ClusterOffset) -> SectorOffset {
        let cluster: u32 = cluster.into();
        debug_assert!(cluster >= 2);
        SectorOffset((cluster - 2) * self.sectors_per_cluster as u32 + self.first_data_sector().0)
    }

    pub fn start_of_cluster(&self, cluster: ClusterOffset) -> ByteOffset {
        ByteOffset(self.first_sector_of_cluster(cluster).0 * self.bytes_per_sector as u32)
    }

    // Get the sector offset of the first root cluster
    pub fn first_root_cluster(&self) -> SectorOffset {
        self.first_sector_of_cluster(self.root_cluster())
    }

    // Get the byte offset of the first root cluster
    pub fn first_root_cluster_bytes(&self) -> ByteOffset {
        ByteOffset(self.first_sector_of_cluster(self.root_cluster()).0 * self.bytes_per_sector as u32)
    }

    pub fn data_sectors(&self) -> u32 {
        self.total_sectors_32 - (self.reserved_sectors as u32 + self.number_of_fats as u32 * self.fat_size_32)
    }

    pub fn count_of_clusters(&self) -> u32 {
        self.data_sectors() / self.sectors_per_cluster as u32
    }

    /// Returns the type of FAT filesystem exposed here.
    pub fn fat_type(&self) -> FatType {
        // ExFAT file systems have bytes 11-64 as all 0s.
        let exfat_zero_region = unsafe {
            mem::transmute::<&u16, &[u8; 53]>(&self.bytes_per_sector)
        };
        if exfat_zero_region.iter().all(|&x| x == 0) {
            return FatType::ExFat;
        }
        match self.count_of_clusters() {
            n if n < 4085 => FatType::Fat12,
            n if n >= 4085 && n < 65525 => FatType::Fat16,
            _ => FatType::Fat32,
        }
    }

    /// Calcualate the total amount of data that can be stored by this FAT32 partition.
    ///
    /// This would require perfect allocation of bytes, which isn't realistic.
    pub fn data_capacity(&self) -> u32 {
        self.bytes_per_sector as u32 * self.sectors_per_cluster as u32 * self.count_of_clusters()
    }

    /// Calculate the total size of the allocated FAT32 partition
    pub fn total_size(&self) -> u32 {
        self.bytes_per_sector as u32 * self.total_sectors_32 as u32
    }

    /// Calculate the sector containing the FAT entry for the given cluster
    pub fn fat_sector_number(&self, cluster: ClusterOffset) -> SectorOffset {
        let fat_offset = cluster * 4;
        (self.reserved_sectors as u32 + Into::<u32>::into(fat_offset) / self.bytes_per_sector as u32).into()
    }

    /// Calculate the entry number for the given cluster within its FatSector
    pub fn entry_offset_in_fat_sector(&self, cluster: ClusterOffset) -> u32 {
        (Into::<u32>::into(cluster) % self.bytes_per_sector as u32).into()
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum FilenameMatch {
    /// The filenames match exactly (uniquely)
    Exact,
    /// The filenames *could* be a possible match
    Possible,
    /// The filenames are not a match
    None,
}

/// Determines if the given filename matches the given short name
// TODO: Move to the `DirectoryEntryShort` struct
pub fn short_name_compare(short_name: &[u8; 11], search_name: &[u8]) -> FilenameMatch {
    // Track if a partial match has been found. This is indicated by running into any characters
    // that are mapped or seeing a tile (~) in the short filename
    let mut partial_match = false;
    println!("Comparing short_name '{:?}' and search_name '{:?}'", short_name, search_name);

    // Find the "true" extension period in a filename. This period cannot be connected to the start
    // or the end of the filename by other periods. So we scan through the filename skipping over a
    // string of periods and then after that it's the last period we find that indicates the start
    // of the file extension.
    let ext_period = {
        let mut leading_period = true;
        let mut period_index = None;
        let mut last_period_index = None;
        let mut ignore_period = false;
        for (i, &c) in search_name.iter().enumerate() {
            println!("{}: {}", i, c);
            if leading_period {
                if c != b'.' {
                    leading_period = false;
                }
            } else if c == b'.' {
                if !ignore_period {
                    last_period_index = period_index;
                    period_index = Some(i);
                    ignore_period = true;
                }
            } else if ignore_period && (c != b' ' && c != b'.') {
                println!("Ignoring search_name[{}] = {}", i, c);
                ignore_period = false;
            }
        }
        if ignore_period {
            last_period_index
        } else {
            period_index
        }
    };
    println!("extension period index: {:?}", ext_period);

    // Current comparison character within `search_name`
    {
        let mut i = 0; // Current character within `short_name`
        for (n, &c) in search_name.iter().enumerate() {

            // We're done parsing the filename if we're at the 8th character.
            if i == 8 {
                break;
            }

            // We're also done searching if we've reached the extension period
            if let Some(period_index) = ext_period {
                if n == period_index {
                    println!("Found extension period");
                    break;
                }
            }

            let s = short_name[i];
            println!("Comparing short_name[{}] '{}' and search_name[{}] '{}'", i, s, n, c);

            let upper = match c {
                // Ignore spaces and indicate precision loss
                b' ' => {
                    println!("space");
                    partial_match = true;
                    continue;
                }
                // All periods found here will be inner periods, not the extension period, so just
                // ignore them.
                b'.' => {
                    println!("inner period");
                    partial_match = true;
                    continue;
                }
                // Convert special characters to underscores and indicate precision loss
                b',' | b'[' | b']' | b';' | b'=' | b'+' => {
                    println!("special");
                    partial_match = true;
                    b'_'
                }
                // Otherwise, for lower-case letters, we upper-case them. Note that FAT32 is a case-
                // preserving but case-insensitive file system. It's part of the spec that all
                // short file names in a directory be unique.
                c if c >= 0x61 && c <= 0x7A  => {
                    c - 0x20 // Convert to uppercase
                }
                _ => c
            };

            println!("Comparing short_name[{}] '{}' and converted search_name[{}] '{}'", i, s, n, upper);
            match s {
                // Once we run into a tilde, move on to processing the extension
                // FIXME: We assume that everything's valid after this point
                b'~' => {
                    println!("Found tilde");
                    partial_match = true;
                    break;
                }
                // A space indicates that this is the end of the filename. If we aren't at the
                // extension period yet (indicating that there's still text to match to the
                // filename) then this is a failed match.
                b' ' => {
                    println!("SPACE!");
                    println!("{:?}, {}", ext_period, n);
                    if let Some(ext_period_index) = ext_period {
                        if n != ext_period_index {
                            return FilenameMatch::None;
                        }
                    }
                    break;
                }
                // Ignore if the characters do match
                _ if upper == s => (),
                _ => return FilenameMatch::None,
            }

            // Otherwise just keep scanning
            i += 1;
        }
    }

    println!("Matched so far! (Partial match: {})", partial_match);

    // If a period was found in the filename, do a match on the extension. Otherwise make sure the
    // extension is empty
    let mut match_good = true;
    let mut partial_ext_match = false;
    if let Some(ext_period_index) = ext_period {
        // If this is the final period, but there's nothing afterwards, it's the same as not having
        // an extension specified at all, so just do that check instead here.
        if ext_period_index == search_name.len() - 1 {
            if short_name[8] != b' ' || short_name[9] != b' ' || short_name[10] != b' ' {
                match_good = false;
            }
        } else {
            let mut ext_index = 0;
            // It's safe here to increment because the last-character case is addressed above.
            for n in (ext_period_index + 1)..search_name.len() {
                let mut c = search_name[n];
                println!("Extension search: search_name[{}] = {}", n, c);
                if ext_index < 3 {
                    c = if c >= 0x61 && c <= 0x7A {
                        c - 0x20 // Convert to uppercase
                    } else {
                        c
                    };
                    let i = 8 + ext_index;
                    let short_name_c = short_name[i];

                    println!("Comparing short_name[{}] '{}' and search_name[{}] '{}'", i, short_name_c, n, c);

                    // If we're done with extension characters, this wasn't a match
                    if short_name_c == b' ' {
                        match_good = false;
                    }
                    // Otherwise if we're still matching, keep going.
                    else if c == short_name_c {
                        match_good = match_good && true;
                    }
                    // But once we find a bad match, it's over.
                    else if c != short_name_c {
                        match_good = false;
                    }

                    ext_index += 1;
                }
                // Once we've gone through 3 extension characters and found more, this is only a partial
                // match and we can stop searching.
                else {
                    partial_ext_match = true;
                    break;
                }
            }
        }
    } else if short_name[8] != b' ' || short_name[9] != b' ' || short_name[10] != b' ' {
        match_good = false;
    }

    if !match_good {
        FilenameMatch::None
    } else if partial_match || partial_ext_match {
        FilenameMatch::Possible
    } else {
        FilenameMatch::Exact
    }
}

/// Short filename checksum generator
fn short_name_checksum(name: &[u8; 11]) -> u8 {
    let mut sum = 0u8;
    for &c in name.iter() {
        sum = sum.rotate_right(1).overflowing_add(c).0;
    }
    sum
}

pub enum FatEntry {
    /// The cluster had been marked as bad and should not be used for storing data.
    BadCluster,
    /// The cluster is unused by any file.
    FreeCluster,
    /// The cluster number of the next link in the cluster chain for a file.
    ClusterReference(ClusterOffset),
}

/// Describes a sector of the file allocation table (FAT)
///
/// This is described as a chunk of bytes because FAT32 is defined as little-endian. Therefore there
/// will need to be some conversion when accessing this data on big-endian platforms.
pub struct FatSector([u8; 512]);

impl FatSector {

    pub fn new(data: [u8; 512]) -> Self {
        FatSector(data)
    }

    /// Return the data stored in the given entry.
    pub fn entry(&self, id: u8) -> FatEntry {
        debug_assert!(id < 128);

        let entry_offset: usize = id as usize * 4;
        let entry = LittleEndian::read_u32(&self.0[entry_offset..]);

        // Zero entries indicate that the corresponding cluster is free.
        if entry == 0 {
            FatEntry::FreeCluster
        }
        // If the cluster has 0xFFFFFF7 in its low bits, it's been marked as bad.
        else if entry & 0x0FFFFFF7 == 0x0FFFFFF7 {
            FatEntry::BadCluster
        }
        // Otherwise this is a valid cluster reference. We trim off the top 4 bits as these are
        // really only 28-bit entries (I dunno why). This also means if any high bits have been set
        // for any reason they should be kept when the entry is updated. For example the entries
        // 0x10000000 and 0xF0000000 are equivalent.
        else {
            FatEntry::ClusterReference(ClusterOffset(entry & 0x0FFFFFFF))
        }
    }
}

/// Describes a sector containing directory entries.
pub struct DirectorySector([u8; 512]);

impl DirectorySector {

    pub fn new(data: [u8; 512]) -> Self {
        DirectorySector(data)
    }

    pub fn entry(&self, id: u8) -> DirectoryEntry {
        debug_assert!(id < 16);
        let entry_offset: usize = id as usize * 32;
        println!("offset: {}, First character: 0x{:X}", entry_offset, self.0[entry_offset]);
        if self.0[entry_offset] == 0xE5 {
            println!("Unused");
            DirectoryEntry::Unused
        } else if self.0[entry_offset] == 0x00 {
            println!("END");
            DirectoryEntry::FirstFree
        } else if self.0[entry_offset + 11] == LONG_NAME.bits() {
            let entry = unsafe {
                DirectoryEntryLong::new(&self.0[entry_offset..])
            };
            // TODO: Validate entry
            DirectoryEntry::Long(entry)
        } else {
            let entry = unsafe {
                DirectoryEntryShort::new(&self.0[entry_offset..])
            };
            // TODO: Validate entry
            DirectoryEntry::Short(entry)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    // Values taken from:
    // http://www.easeus.com/data-recovery-ebook/root-directory-management-in-FAT32.htm
    #[test]
    fn test_short_name_checksum1() {
        assert_eq!(short_name_checksum(&b"LONGFI~1TXT"), 0xD4);
    }
    #[test]
    fn test_short_name_checksum2() {
        assert_eq!(short_name_checksum(&b"THEQUI~1FOX"), 0x07);
    }

    // TODO: Add tests for mapping full filenames into DIR_Name from
    // http://home.teleport.com/~brainy/fatgen102.pdf, page 23

    // All entries taken from
    // http://read.pudn.com/downloads77/ebook/294884/FAT32%20Spec%20(SDA%20Contribution).pdf
    #[test]
    fn test_short_name_compare_lowercase() {
        let short_name = b"FOO     BAR";
        let search_name = b"foo.bar";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_uppercase() {
        let short_name = b"FOO     BAR";
        let search_name = b"FOO.BAR";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_mixedcase() {
        let short_name = b"FOO     BAR";
        let search_name = b"Foo.Bar";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_noext1() {
        let short_name = b"FOO        ";
        let search_name = b"foo";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_noext2() {
        let short_name = b"FOO        ";
        let search_name = b"foo.";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_shortext() {
        let short_name = b"PICKLE  A  ";
        let search_name = b"PICKLE.A";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_max() {
        let short_name = b"PRETTYBGBIG";
        let search_name = b"prettybg.big";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    // The following test is listed in this document, but I don't think it's correct. Elsewhere in
    // the document it specifies that leading periods are allowed and stored in the long name. So I
    // think that extension-only files don't technically exist. I've therefore modified this test to
    // what I think is correct behavior.
    #[test]
    fn test_short_name_compare_ext_only() {
        let short_name = b"BIG        ";
        let search_name = b".big";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_long() {
        let short_name = b"THEQUI~1FOX";
        let search_name = b"The quick brown.fox";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    // The following are taken from:
    // https://en.wikipedia.org/wiki/8.3_filename

    #[test]
    fn test_short_name_compare_leading_period() {
        let short_name = b"BASHRC~1SWP";
        let search_name = b".bashrc.swp";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    /// When there are many duplicate filesnames for the shortname, double-digits are used
    #[test]
    fn test_short_name_compare_many_duplicates() {
        let short_name = b"TEXTF~10TXT";
        let search_name = b"TextFile.Mine.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_invalid_short_chars() {
        let short_name = b"VER_12~1TEX";
        let search_name = b"ver +1.2.text";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    // And from https://support.microsoft.com/en-us/help/142982/how-windows-generates-8.3-file-names-from-long-file-names

    #[test]
    fn test_short_name_compare_multiple_periods1() {
        let short_name = b"THISIS~1TXT";
        let search_name = b"This is a really long filename.123.456.789.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_multiple_periods2() {
        let short_name = b"THISIS~1789";
        let search_name = b"This is a really long filename.123.456.789.";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    // Tests made up. Truthiness determined from experimenting with Windows 8.1 directly

    #[test]
    fn test_short_name_compare_leading_period_no_ext1() {
        let short_name = b"BASHRC~1   ";
        let search_name = b".bashrc";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_leading_period_no_ext2() {
        let short_name = b"BASHRC~1   ";
        let search_name = b"..bashrc";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_multiple_periods3() {
        let short_name = b"THISIS~1789";
        let search_name = b"This is a really long filename.123.456.789.   ";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_multiple_periods4() {
        let short_name = b"THISIS~1789";
        let search_name = b"This is a really long filename.123.456.789....";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_exact_short() {
        let short_name = b"HEY     TXT";
        let search_name = b"hey.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Exact);
    }

    #[test]
    fn test_short_name_compare_long_search_name() {
        let short_name = b"HEY     TXT";
        let search_name = b"heythere.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::None);
    }

    #[test]
    fn test_short_name_compare_close_short() {
        let short_name = b"HEY     TXT";
        let search_name = b"hey4.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::None);
    }

    #[test]
    fn test_short_name_compare_short_space_in_name() {
        let short_name = b"HEY~1   TXT";
        let search_name = b"hey .txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    #[test]
    fn test_short_name_compare_short_plus_in_name() {
        let short_name = b"HEY~1   TXT";
        let search_name = b"hey+.txt";
        assert_eq!(short_name_compare(&short_name, search_name), FilenameMatch::Possible);
    }

    // Tests for DirectoryEntryShort

    // Specified by fatgen103.doc
    #[test]
    fn test_dir_entry_short_leading_period() {
        let dir_entry = DirectoryEntryShort {
            name: *b".BAR       ",
            attributes: FileAttributes::empty(),
            nt_reserved: 0,
            creation_time_tenth: 0,
            creation_time: 0,
            creation_date: 0,
            last_access_date: 0,
            first_cluster_high: 0,
            last_write_time: 0,
            last_write_date: 0,
            first_cluster_low: 0,
            file_size: 0,
        };
        assert!(!dir_entry.is_valid());
    }

    // Invalid because all directory entries should have a file size of 0
    #[test]
    fn test_dir_entry_short_nonzero_size_directory() {
        let dir_entry = DirectoryEntryShort {
            name: *b"DIR1       ",
            attributes: DIRECTORY,
            nt_reserved: 0,
            creation_time_tenth: 0,
            creation_time: 0,
            creation_date: 0,
            last_access_date: 0,
            first_cluster_high: 0,
            last_write_time: 0,
            last_write_date: 0,
            first_cluster_low: 0,
            file_size: 11,
        };
        assert!(!dir_entry.is_valid());
    }

    // Invalid because the short name can't have lowercase letters
    #[test]
    fn test_dir_entry_short_lowercase_short_name() {
        let dir_entry = DirectoryEntryShort {
            name: *b"dir1       ",
            attributes: DIRECTORY,
            nt_reserved: 0,
            creation_time_tenth: 0,
            creation_time: 0,
            creation_date: 0,
            last_access_date: 0,
            first_cluster_high: 0,
            last_write_time: 0,
            last_write_date: 0,
            first_cluster_low: 0,
            file_size: 0,
        };
        assert!(!dir_entry.is_valid());
    }
}
