extern crate fat;

use std::{fmt, mem};
use std::fs::File;
use std::io::prelude::*;
use std::str;

use fat::*;

struct FileDevice {
    data: Vec<u8>,
}

impl fmt::Debug for FileDevice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "FileDevice {{")?;

        write!(f, " size: {} bytes ", self.data.len())?;

        write!(f, "}}")
    }
}

impl StorageDevice for FileDevice {
    fn initialize(id: u8) -> FatResult<Self> {
        let image_path = match id {
            0 => "fat32.img",
            _ => return Err(FatError::InvalidDeviceId)
        };
        let mut file = File::open(image_path).map_err(|_| FatError::IoError)?;
        let file_size = file.metadata().map_err(|_| FatError::IoError)?.len();
        println!("Test file {} bytes", file_size);

        // Read entire file for simplicity
        let mut file_contents = Vec::new();
        let data_read = file.read_to_end(&mut file_contents)
                            .map_err(|_| FatError::IoError)?;
        assert_eq!(file_size, data_read as u64);

        Ok(FileDevice {
            data: file_contents,
        })
    }

    fn status(&self, id: u8) -> FatResult<DeviceStatus> {
        Err(FatError::InvalidDeviceId)
    }

    fn read(&mut self, id: u8, buffer: &mut [u8], sector: SectorOffset, num_sectors: u8) -> FatResult<()> {
        let sector: u32 = sector.into();
        let start_byte = (sector * 512) as usize;
        let end_byte = ((sector + num_sectors as u32) * 512) as usize;
        buffer.copy_from_slice(&self.data[start_byte..end_byte]);
        Ok(())
    }

    fn write(&mut self, id: u8, buffer: &[u8], sector: SectorOffset, num_sectors: u8) -> FatResult<()> {
        Err(FatError::IoError)
    }
}

fn check_file<T: StorageDevice>(fs: &mut FatFileSystem<T>, path: &str) {
    if let Ok(x) = fs.root().open_file(&path) {
        if let Some(x) = x {
            println!("Found '{}'!", &path);
            println!("{:?}", x);
        } else {
            println!("Didn't find '{}'!", &path);
        }
    } else {
        println!("Failure searching fior '{}'!", &path);
    }
}

fn check_dir<T: StorageDevice>(fs: &mut FatFileSystem<T>, path: &str) {
    if let Ok(x) = fs.root().open_directory(&path) {
        if let Some(x) = x {
            println!("Found '{}'!", &path);
            println!("{:?}", x);
        } else {
            println!("Didn't find '{}'!", &path);
        }
    } else {
        println!("Failure searching fior '{}'!", &path);
    }
}

fn main() {
    let mut fs = FatFileSystem::<FileDevice>::mount(2).unwrap();
    let mut root = fs.root();
    {
        let mut test_file = root.open_file("hey.txt").unwrap().unwrap();
        while !test_file.at_end() {
            let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
            test_file.read(&mut buf).unwrap();
            println!("{:?}", &buf);
            println!("{}", str::from_utf8(&buf).unwrap());
        }
        println!("{:?}", test_file);
    }
    {
        let mut test_file = root.open_file("hey.txt").unwrap().unwrap();
        let mut buf: [u8; 28] = unsafe { mem::uninitialized() };
        test_file.read(&mut buf).unwrap();
        println!("{:?}", &buf);
        println!("{}", str::from_utf8(&buf).unwrap());
        assert!(test_file.at_end());
    }
}
